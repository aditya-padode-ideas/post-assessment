package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class MeetingSchedule {

	private final HashMap<String, Set<LocalDateTime>> meetingsPerEmployee = new HashMap<>();

	public static final String MINUTE_00 = ":00";
	public static final DateTimeFormatter YYYY_MM_DD_HH = DateTimeFormatter.ofPattern("yyyy MM dd HH");
	public static final String NO_SLOTS_AVAILABLE = "No Slots available";
	public static final String SUCCESS_MSG = "Meeting Scheduled Successfully at ";

	public String scheduleBefore(Set<String> attendees, LocalDateTime endTime) {
		LocalDateTime earliestAvailableHour = findEarliestAvailableHour(attendees);
		if (earliestAvailableHour.isAfter(endTime)) {
			return NO_SLOTS_AVAILABLE;
		}
		endTime = earliestAvailableHour;
		for (String attendee : attendees) {
			addMeeting(endTime, attendee);
		}
		return SUCCESS_MSG + YYYY_MM_DD_HH.format(endTime) + MINUTE_00;
	}

	private LocalDateTime findEarliestAvailableHour(Set<String> attendees) {
		Set<LocalDateTime> attendeesBusyHours = busyHours(attendees);
		LocalDateTime min = LocalDateTime.now();
		min = skipNonBusinessHours(min);
		if (attendeesBusyHours.isEmpty()) {
			return min;
		}
		min = Arrays.asList(min, Collections.min(attendeesBusyHours)).stream().sorted()
				.max(LocalDateTime::compareTo).get();
		LocalDateTime max = Collections.max(attendeesBusyHours).withMinute(1);
		min = min.withMinute(0).withSecond(0).withNano(0);
		while (!min.isAfter(max)) {
			if (!attendeesBusyHours.contains(min)) {
				break;
			}
			min = min.plusHours(1);
			min = skipNonBusinessHours(min);
		}
		return min;
	}

	private LocalDateTime skipNonBusinessHours(LocalDateTime min) {
		if (min.getHour() > 16 || min.getHour() < 8) {
			min = min.plusDays(1).withHour(8);
		}
		return min;
	}

	private Set<LocalDateTime> busyHours(Set<String> attendees) {
		return attendees.stream()
				.filter(a -> meetingsPerEmployee.containsKey(a))
				.map(a -> meetingsPerEmployee.get(a))
				.flatMap(Set::stream)
				.collect(Collectors.toSet());
	}

	private void addMeeting(LocalDateTime time, String attendee) {
		Set<LocalDateTime> busyHours = meetingsPerEmployee.get(attendee);
		if (busyHours == null) {
			busyHours = new HashSet<>();
		}
		busyHours.add(time = time.withMinute(0).withSecond(0).withNano(0));
		meetingsPerEmployee.put(attendee, busyHours);
	}

}
