package com.xnsio.cleancode;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class MeetingScheduleTest {
	private final Set<String> attendeesTomAndJerry = new HashSet<>(Arrays.asList("Tom","Jerry"));
	private final LocalDateTime tommorow11Am = LocalDate.now().plusDays(1).atTime(11,0);
	private final LocalDateTime tommorow12Am = LocalDate.now().plusDays(1).atTime(12,0);
	private  MeetingSchedule meetingSchedule;
	private final Set<String> attendeesSaunAndJohn= new HashSet<>(Arrays.asList("Saun","John"));
	private final Set<String> attendeesTomAndMarry= new HashSet<>(Arrays.asList("Tom","Marry"));;

	@Before
	public void setUp() {
		meetingSchedule = new MeetingSchedule();
	}

	@Test
	public void scheduleMeetingInFreeBusinessHours() throws Exception {
		assertEquals(successMessageWith(upcomingBusinessHour()), meetingSchedule.scheduleBefore(attendeesTomAndJerry,tommorow11Am));
	}


	@Test
	public void scheduleMeetingInEarliestAvailableHours() throws Exception {
		scheduleMeetingsUpto(tommorow11Am);
		LocalDateTime tommorow1Pm = LocalDate.now().plusDays(1).atTime(13, 0);
		assertEquals(successMessageWith(tommorow12Am), meetingSchedule.scheduleBefore(attendeesTomAndJerry,tommorow1Pm));
	}

	@Test
	public void dontScheduleIfTimeNotAvailable() throws Exception {
		scheduleMeetingsUpto(tommorow12Am);
		assertEquals(MeetingSchedule.NO_SLOTS_AVAILABLE, meetingSchedule.scheduleBefore(attendeesTomAndJerry,tommorow11Am));
	}

	@Test
	public void scheduleForFreePersonsOnly() throws Exception {
		scheduleMeetingsUpto(tommorow11Am);
		assertEquals(successMessageWith(upcomingBusinessHour()), meetingSchedule.scheduleBefore(attendeesSaunAndJohn,tommorow12Am));
		assertEquals(successMessageWith(tommorow12Am), meetingSchedule.scheduleBefore(attendeesTomAndMarry,tommorow12Am));
	}

	private String successMessageWith(LocalDateTime upcomingBusinessHour) {
		return MeetingSchedule.SUCCESS_MSG+MeetingSchedule.YYYY_MM_DD_HH.format(upcomingBusinessHour)+MeetingSchedule.MINUTE_00;
	}

	@Test
	public void dontScheduleExceptBusinessHours() throws Exception {
		LocalDateTime tommorow5Pm = LocalDate.now().plusDays(1).atTime(17,0);;
		scheduleMeetingsUpto(tommorow5Pm);
		LocalDateTime tommorow6Pm = LocalDate.now().plusDays(1).atTime(18,0);;
		assertEquals(MeetingSchedule.NO_SLOTS_AVAILABLE, meetingSchedule.scheduleBefore(attendeesTomAndJerry,tommorow6Pm));
	}

	private void scheduleMeetingsUpto(LocalDateTime endTime) {
		LocalDateTime now = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0);
		while(!now.isAfter(endTime)){
			meetingSchedule.scheduleBefore(attendeesTomAndJerry, now);
			now=now.plusHours(1);
		}
	}

	private LocalDateTime upcomingBusinessHour() {
		LocalDateTime now = LocalDateTime.now();
		if(now.getHour()>17 || now.getHour()<8){
			now = now.plusDays(1).withHour(8);
		}
		return now;
	}
}
